#!/bin/sh

if [[ -z "${SERVER_PORT}" ]]; then
	exec /go/bin/shadowsocks-server -c /etc/shadowsocks/config.json
else
	exec /go/bin/shadowsocks-server -p ${SERVER_PORT} -m ${ENCRYPTION} -k ${PASSWORD} 
fi
