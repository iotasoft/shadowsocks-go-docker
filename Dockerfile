FROM golang:1.10.1-alpine

LABEL description="shadowsocks-go server docker image" maintainer="Amin Khozaei <ariotasoft@gmail.com> (@ariota)"

RUN apk update && apk add --no-cache git

RUN go get github.com/shadowsocks/shadowsocks-go/cmd/shadowsocks-server

ADD start.sh /usr/local/bin/start.sh

RUN chmod +x /usr/local/bin/start.sh

ENV ENCRYPTION=aes-256-cfb

ENTRYPOINT ["/bin/sh", "-c", "/usr/local/bin/start.sh"]
